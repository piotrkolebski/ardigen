# ardigen



## Getting started

Before development:
```bash
pip install pre-commit
pre-commit install
pip install -e .
```

## Training
To train a new model you should crete new merge request and commit training configuration in `ardigen/configs/cinfog.json`.
After training, you can download model checkpoint, model exported to onnx format and training metrics (accuracy, loss, auc and f1 score) as tensorboard logs from `CI/CD -> Pipelines -> three dots on latest pipeline`

## Deployment
Newly trained models are always deployed as docker image to `Gitlab Container Registry`. It is not deployed to remote machine because I did not get access to any.
To run backend and frontend application:
```bash
docker-compose -f docker/docker-compose.yml up
```
and open [link](http://localhost) in the browser.

Also, model is accessible via HTTP requests, ex.:
```bash
curl -X POST "http://localhost:5000/predict" \
-H "accept: */*" \
-H "Content-Type: application/json" \
-d "{\"HLA type\":\"A02:01\",\"peptide sequence\":\"KLWDLLYKL\"}
```

Response:
```
[
  [
    0.15428099036216736
  ]
]
```

## What can be done in the future
- data versioning (e.x. DVC)
- models export and versioning (e.x. on AWS S3)

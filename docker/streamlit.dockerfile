FROM python:3.8-buster

WORKDIR app/

COPY ardigen/presentation/gui ./

RUN pip install streamlit

EXPOSE 8501

CMD streamlit run main.py

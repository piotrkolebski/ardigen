import numpy as np
import pandas as pd


def prepare_dataset(dataset: pd.DataFrame, hla_encoder):
    hla_encoder.fit(dataset.HLA_type.values.reshape(-1, 1))
    hla_types = hla_encoder.transform(dataset.HLA_type.values.reshape(-1, 1)).toarray()
    letter2ind = {"#": 0}
    for seq in dataset.peptide_sequence:
        for letter in seq:
            if letter not in letter2ind:
                letter2ind[letter] = len(letter2ind)
    sequences = dataset.peptide_sequence.apply(
        lambda x: [letter2ind[letter] for letter in x]
    )
    max_len = len(max(sequences, key=lambda x: len(x)))
    sequences = sequences.apply(lambda x: x + ([0] * (max_len - len(x))))
    return (
        (hla_types, np.array(sequences)),
        np.array(dataset.label),
        letter2ind,
        hla_encoder,
    )

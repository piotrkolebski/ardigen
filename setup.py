from setuptools import find_packages, setup

setup(
    name="Ardigen",
    packages=find_packages(),
    install_requires=[
        "BentoML==0.13.1",
        "click==7.1.2",
        "numpy==1.21.2",
        "onnx==1.10.1",
        "onnxruntime==1.9.0",
        "pandas==1.3.3",
        "Pillow==8.3.2",
        "pre-commit==2.15.0",
        "pytorch-lightning==1.4.9",
        "requests==2.26.0",
        "scikit-learn==1.0",
        "scipy==1.7.1",
        "streamlit==1.0.0",
        "tensorboard==2.6.0",
        "torch==1.9.1",
        "tqdm==4.62.3",
    ],
)

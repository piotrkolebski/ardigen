import torch
from torch.utils.data import Dataset


class SimpleDataset(Dataset):
    def __init__(self, xs, ys):
        self.xs = xs
        self.labels = ys

    def __getitem__(self, item):
        xs1_tensor = torch.tensor(self.xs[0][item]).float()
        xs2_tensor = torch.tensor(self.xs[1][item])
        return (xs1_tensor, xs2_tensor), self.labels[item]

    def __len__(self):
        return len(self.xs[0])

import torch
import torch.nn as nn

from ardigen.models.base_model import BaseModel


class SimpleModel(BaseModel):
    def __init__(self, embedding_dim=15, lstm_hidden_size=8, linear1_a_out=1):
        super().__init__()
        self.linear1_a = nn.Linear(
            in_features=16,
            out_features=linear1_a_out,
        )
        self.embedding = nn.Embedding(
            num_embeddings=21,
            embedding_dim=embedding_dim,
        )
        self.lstm = nn.LSTM(
            input_size=embedding_dim,
            hidden_size=lstm_hidden_size,
            num_layers=1,
            batch_first=True,
            bidirectional=True,
        )
        self.linear2 = nn.Linear(2 * lstm_hidden_size + linear1_a_out, 1)
        self.sigmoid = nn.Sigmoid()

    def __call__(self, x, *args, **kwargs):
        if isinstance(x, list) and len(x) == 1:
            x = x[0]
        out1_a = self.linear1_a(x[0].float())
        embeddings = self.embedding(x[1].long())
        lstm, _ = self.lstm(embeddings)
        out1 = torch.cat([out1_a, lstm[:, -1, :]], dim=1)
        out2 = self.linear2(out1)
        return self.sigmoid(out2)

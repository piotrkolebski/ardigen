from torch import nn

MODELS = []


class BaseModel(nn.Module):
    def __init_subclass__(cls, **kwargs):
        MODELS.append((cls, cls.__name__))

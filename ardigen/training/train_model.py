import json
import os
import pickle
from typing import Optional, Tuple

import click
import pandas as pd
import pytorch_lightning
import torch
from pytorch_lightning import Trainer, loggers
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.utilities.types import (
    EPOCH_OUTPUT,
    EVAL_DATALOADERS,
    TRAIN_DATALOADERS,
)
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from torch import nn
from torch.optim import Adam
from torch.utils.data import DataLoader

from ardigen.datasets.simple_dataset import SimpleDataset
from ardigen.models import MODELS
from ardigen.saved_models import SAVED_MODELS_PATH
from utils.prepare_dataset import prepare_dataset


class Training(pytorch_lightning.LightningModule):
    def __init__(
        self,
        data_path: str,
        model: nn.Module,
        threshold: float,
        optimizer,
        optimizer_kwargs,
        *args,
        **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.save_hyperparameters()
        self.model = model
        self.optimizer = optimizer(model.parameters(), **optimizer_kwargs)
        data = pd.read_csv(data_path)
        enc_hla_type_encoder = OneHotEncoder(handle_unknown="ignore")

        xs, ys, letter2ind, enc_hla_type_encoder = prepare_dataset(
            data, enc_hla_type_encoder
        )
        to_save = {"encoder": enc_hla_type_encoder, "letter2ind": letter2ind}
        with open("ardigen/output/encoders.pkl", "wb") as file:
            pickle.dump(to_save, file)

        train_l, test_l = train_test_split(
            range(len(ys)),
            test_size=0.2,
            random_state=42,
            stratify=ys,
        )
        train_l, val_l = train_test_split(
            train_l,
            test_size=0.25,
            random_state=42,
            stratify=ys[train_l],
        )
        self.x_train = (xs[0][train_l], xs[1][train_l])
        self.y_train = ys[train_l]

        self.x_test = (xs[0][test_l], xs[1][test_l])
        self.y_test = ys[test_l]

        self.x_val = (xs[0][val_l], xs[1][val_l])
        self.y_val = ys[val_l]

        self.loss = nn.BCELoss()
        self.threshold = threshold

    def train_dataloader(self) -> TRAIN_DATALOADERS:
        return DataLoader(SimpleDataset(self.x_train, self.y_train), batch_size=160)

    def test_dataloader(self) -> EVAL_DATALOADERS:
        return DataLoader(SimpleDataset(self.x_test, self.y_test), batch_size=160)

    def val_dataloader(self) -> EVAL_DATALOADERS:
        return DataLoader(SimpleDataset(self.x_val, self.y_val), batch_size=160)

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        x, y = batch
        x = self.forward(x)
        loss = self.loss(x, y.view(-1, 1).float())
        self.log("train/loss", loss, on_epoch=True)
        return {"loss": loss, "x": x, "y": y}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        x = self.forward(x)
        loss = self.loss(x, y.view(-1, 1).float())
        self.log("validation/loss", loss, on_epoch=True)
        return {"loss": loss, "x": x, "y": y}

    def test_step(self, batch, batch_idx):
        x, y = batch
        x = self.forward(x)
        loss = self.loss(x, y.view(-1, 1).float())
        self.log("test/loss", loss, on_epoch=True)
        return {"loss": loss, "x": x, "y": y}

    def training_epoch_end(self, outputs: EPOCH_OUTPUT) -> None:
        accuracy, f1, auc = self.calculate_metrics(outputs)
        self.log("train/accuracy", accuracy, on_epoch=True)
        self.log("train/f1", f1, on_epoch=True)
        if auc is not None:
            self.log("train/auc", auc, on_epoch=True)

    def validation_epoch_end(self, outputs: EPOCH_OUTPUT) -> None:
        accuracy, f1, auc = self.calculate_metrics(outputs)
        self.log("validation/accuracy", accuracy, on_epoch=True)
        self.log("validation/f1", f1, on_epoch=True)
        if auc is not None:
            self.log("validation/auc", auc, on_epoch=True)

    def test_epoch_end(self, outputs: EPOCH_OUTPUT) -> None:
        accuracy, f1, auc = self.calculate_metrics(outputs)
        self.log("test/accuracy", accuracy, on_epoch=True)
        self.log("test/f1", f1, on_epoch=True)
        if auc is not None:
            self.log("test/auc", auc, on_epoch=True)

    def configure_optimizers(self):
        return self.optimizer

    def calculate_metrics(self, outputs) -> Tuple[float, float, Optional[float]]:
        xs = [batch["x"] for batch in outputs]
        xs = [x.numpy() for batch in xs for x in batch]
        xs_label = [i > self.threshold for i in xs]
        ys = [batch["y"] for batch in outputs]
        ys = [y.numpy() for batch in ys for y in batch]
        accuracy = accuracy_score(ys, xs_label)
        f1 = f1_score(ys, xs_label)
        try:
            auc = roc_auc_score(ys, xs)
        except ValueError:
            return accuracy, f1, None
        print(accuracy, f1, auc)
        return accuracy, f1, auc


@click.command()
@click.option(
    "--config_path",
    default="ardigen/configs/config.json",
    help="Path to training configuration",
)
@click.option(
    "--dataset_path",
    default="ardigen/datasets/dataset.csv",
    help="Path to dataset *.csv file",
)
def run_training(
    dataset_path: str,
    config_path: str,
    logging_path: str = "ardigen/output/",
    torchscript_path: Optional[str] = "ardigen/output/out.onnx",
    optimizer_class: torch.optim.Optimizer = Adam,
):
    os.makedirs("ardigen/output", exist_ok=True)
    with open(config_path, "r") as file:
        config = json.load(file)

    model_cls = [m[0] for m in MODELS if m[1] == config["model"]["name"]][0]
    model = model_cls(**config["model"]["config"])
    t = Training(
        dataset_path,
        model,
        config["threshold"],
        optimizer_class,
        config["optimizer_kwargs"],
    )
    checkpoint_callback = ModelCheckpoint(
        monitor="validation/loss", dirpath="ardigen/output", filename="best_model"
    )
    trainer = Trainer(
        default_root_dir=SAVED_MODELS_PATH,
        max_epochs=config["max_epochs"],
        logger=loggers.TensorBoardLogger(logging_path, "logging"),
        callbacks=[checkpoint_callback],
    )
    trainer.fit(t)
    trainer.test(t)
    if torchscript_path:
        sample_input = next(
            iter(DataLoader(SimpleDataset(t.x_val, t.y_val), batch_size=1))
        )[0]
        t.to_onnx(torchscript_path, sample_input, export_params=True)


if __name__ == "__main__":
    print(os.getcwd())
    run_training()

import pickle

import click
import onnx

from ardigen.presentation.model_deployment.bento_model import BindingClassifier


@click.command()
@click.option(
    "--model_path", default="ardigen/output/out.onnx", help="Path to exported model"
)
@click.option(
    "--encoders_path",
    default="ardigen/output/encoders.pkl",
    help="Path to data encoders file",
)
def build_backend(model_path: str, encoders_path: str):
    binding_classifier_service = BindingClassifier()
    ort_session = onnx.load_model(model_path)
    binding_classifier_service.pack("model", ort_session)
    with open(encoders_path, "rb") as file:
        encoders = pickle.load(file)
    from sklearn.preprocessing import OrdinalEncoder

    enc = OrdinalEncoder()
    letter2ind = [[k, v] for k, v in encoders["letter2ind"].items()]
    enc.fit(letter2ind)
    binding_classifier_service.pack("letter2ind", enc)
    binding_classifier_service.pack("encoder", encoders["encoder"])
    binding_classifier_service.save()


if __name__ == "__main__":
    build_backend()

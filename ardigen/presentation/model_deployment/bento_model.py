import numpy as np
from bentoml import BentoService, api, artifacts, env
from bentoml.adapters import JsonInput
from bentoml.frameworks.onnx import OnnxModelArtifact
from bentoml.frameworks.sklearn import SklearnModelArtifact


@env(infer_pip_packages=True)
@artifacts(
    [
        OnnxModelArtifact("model"),
        SklearnModelArtifact("encoder"),
        SklearnModelArtifact("letter2ind"),
    ]
)
class BindingClassifier(BentoService):
    @api(input=JsonInput(), batch=True)
    def predict(self, data):
        inputs = self.artifacts.model.get_inputs()
        print(data)
        data = data[0]
        categories = self.artifacts.letter2ind.categories_
        letter2ind = {k: v for k, v in zip(*categories)}
        sequence = [letter2ind[x] for x in data["peptide sequence"]]
        sequence = [sequence + ([0] * (11 - len(sequence)))]
        ort_inputs = {
            inputs[0]
            .name: self.artifacts.encoder.transform([[data["HLA type"]]])
            .toarray()
            .astype(np.float32),
            inputs[1].name: sequence,
        }
        print(ort_inputs)
        return self.artifacts.model.run(None, ort_inputs)

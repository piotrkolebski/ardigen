import os

import requests
import streamlit as st

BACKEND_URL = os.getenv("BACKEND_URL", "http://backend:5000/predict")

st.title("Ardigen")

hla_type = st.text_input("HLA type")
sequence = st.text_input("Peptide sequence")

if st.button("Predict"):
    data = {
        "HLA type": hla_type,
        "peptide sequence": sequence,
    }
    response = requests.get(BACKEND_URL, json=data)
    st.write(f"Probability: {response.json()[0][0] * 100:.2f}%")
